@extends('layouts.master')

@section('title','Edit Category')

@section('content')
<div class="m-5">
    <form method="POST" action="{{ url('category/'.$category->id) }}">
        @csrf
        <input type="hidden" name="_method" value="PUT">
        <div class="form-group">
            <label for="title">Nama Category</label>
            <input class="form-control" placeholder="Nama Category" name="nama_category" value="{{ $category->nama_category }}" required autocomplete="">
        </div>
        <div class="form-group">
            <label for="slug">Slug</label>
            <input class="form-control" placeholder="Slug" name="slug" value="{{ $category->slug }}" required autocomplete="">
        </div>
        <div class="form-group">
            <label for="desc">Description</label>
            <input class="form-control" placeholder="Description" name="description" value="{{ $category->description }}" required autocomplete="">
        </div>
        
        <div class="form-group">
            <input type="submit" class="btn btn-primary" value="Simpan Data">
        </div>
    </form>
</div>
@endsection