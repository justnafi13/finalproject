@extends('layouts.master')

@section('title','Dashboard')

@section('content')
<div class="m-5">
    <form method="POST" action="{{ url('users/'.$users->id) }}">
        @csrf
        <input type="hidden" name="_method" value="PUT">
        <!-- <input type="hidden" name="user_id" value="{{ $users->id }}"> -->
        <div class="form-group">
            <label for="title">Username</label>
            <input class="form-control  @error('name') is-invalid @enderror" placeholder="Username" name="name" value="{{ $users->name }}" required autocomplete="Username">

            @error('name')
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
            @enderror
        </div>
        <div class="form-group">
            <label for="title">Email</label>
            <input class="form-control  @error('email') is-invalid @enderror" placeholder="Email" name="email" value="{{ $users->email }}" required autocomplete="Email">

            @error('email')
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
            @enderror
        </div>
        <div class="form-group">
            <label for="title">Password</label>
            <input class="form-control  @error('password') is-invalid @enderror" placeholder="Password" name="password" value="{{ $users->judul }}" required autocomplete="Password">

            @error('password')
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
            @enderror
        </div>
        <div class="form-group">
            <input type="submit" class="btn btn-primary" value="Simpan Data">
        </div>
    </form>
</div>
@endsection