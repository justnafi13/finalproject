@extends('layouts.master')

@section('title','Dashboard')

@section('content')
<div class="m-5">
    <form method="POST" action="{{ url('article') }}">
        @csrf
        <div class="form-group">
            <label for="title">Title</label>
            <input class="form-control  @error('name') is-invalid @enderror" placeholder="Title" name="title" required autocomplete="Username">
        </div>
        <div class="form-group">
            <label for="sel1">Select list:</label>
            <select class="form-control" name="category">
                @foreach($category as $row)
                <option value="{{ $row->id }}">{{ $row->nama_category }}</option>
                @endforeach
            </select>
        </div>
        <div class="form-group">
            <label for="title">Slug</label>
            <input class="form-control  @error('name') is-invalid @enderror" placeholder="Slug" name="slug" required autocomplete="Username">
        </div>
        <div class="form-group">
            <label for="title">Content</label>
                <textarea name="content" id="summary-ckeditor" cols="30" rows="10" class="form-control" placeholder="Masukan Content"></textarea>
        </div>
        <input type="hidden" name="_method" name="admin_id" value="">
        <div class="form-group">
            <input type="submit" class="btn btn-primary" value="Simpan Data">
        </div>
    </form>
</div>
@endsection

@section('script')
<script src="//cdn.ckeditor.com/4.14.0/standard/ckeditor.js"></script>
<script>
CKEDITOR.replace( 'summary-ckeditor' );
</script>
@endsection