@extends('layouts.master')

@section('title','Admin')

@section('content')
<div class="card">
    <div class="card-header">
    <h3 class="card-title">Data Admin</h3>
    </div>
    <!-- /.card-header -->
    <div class="card-body">
    <div class="">
        <a href="{{ url('admin/create') }}" class="btn btn-primary mb-3" role="button">Tambah Data</a>
    </div>
    <table id="example1" class="table table-bordered table-striped">
        <thead>
            <tr>
                <th>Id</th>
                <th>Email</th>
                <th>Paasword</th>
                <th>Created</th>
                <th>Updated</th>
            </tr>
        </thead>
        <tbody>
            @foreach($administrator as $p)
            <tr>
                <td>{{ $p->id }}</td>
                <td>{{ $p->name }}</td>
                <td>{{ $p->email }}</td>
                <td>{{ $p->created_at }}</td>
                <td>{{ $p->updated_at }}</td>
            </tr>
            @endforeach
        </tbody>
        </table>
        </div>
        <!-- /.card-body -->
</div>
@endsection

