@extends('layouts.master')

@section('title','User')

@section('content')
<div class="card">
    <div class="card-header">
    <h3 class="card-title">Data Pertanyaan</h3>
    </div>
    <!-- /.card-header -->
    <div class="card-body">
    <div class="">
        <a href="{{ url('users/create') }}" class="btn btn-primary mb-3" role="button">Tambah Data</a>
    </div>
    <table id="example1" class="table table-bordered table-striped">
        <thead>
            <tr>
                <th>Id</th>
                <th>Email</th>
                <th>Paasword</th>
                <th>Created</th>
                <th>Updated</th>
                <th colspan="2" class="text-center">Action</th>
            </tr>
        </thead>
        <tbody>
            @foreach($users as $p)
            <tr>
                <td>{{ $p->id }}</td>
                <td><a href="/users/{{ $p->id }}" target="_blank" rel="noopener noreferrer">{{ $p->name }}</a></td>
                <td>{{ $p->email }}</td>
                <td>{{ $p->created_at }}</td>
                <td>{{ $p->updated_at }}</td>
                <td>
                    <a class="btn" href="/users/{{ $p->id }}/edit"><i class="fas fa-edit"></i></a>
                </td>
                <td>
                <form action="/users/{{ $p->id }}" method="POST">
                @csrf
                    <input type="hidden" name="_method" value="DELETE">
                    <button class="btn"><i class="fas fa-eraser"></i></button>
                </form>
                </td>
            </tr>
            @endforeach
        </tbody>
        </table>
        </div>
        <!-- /.card-body -->
</div>
@endsection

