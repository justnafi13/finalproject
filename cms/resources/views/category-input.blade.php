@extends('layouts.master')

@section('title','Input Category')

@section('content')
<div class="m-5">
    <form method="POST" action="{{ url('category') }}">
        @csrf
        <div class="form-group">
            <label for="title">Nama Category</label>
            <input class="form-control" placeholder="Nama Category" name="nama_category" value="{{ old('nama_category') }}" required autocomplete="">
        </div>
        <div class="form-group">
            <label for="slug">Slug</label>
            <input class="form-control" placeholder="Slug" name="slug" value="{{ old('slug') }}" required autocomplete="">
        </div>
        <div class="form-group">
            <label for="desc">Description</label>
            <input class="form-control" placeholder="Description" name="description" value="{{ old('description') }}" required autocomplete="">
        </div>
        
        <div class="form-group">
            <input type="submit" class="btn btn-primary" value="Simpan Data">
        </div>
    </form>
</div>
@endsection