@extends('layouts.master')

@section('title','Category')

@section('content')
<div class="card text-center">
  <div class="card-header">
    {{ $article->title }}
  </div>
  <div class="card-body">
    <p class="card-text">{!! $article ->content !!}</p>
  </div>
</div>
@endsection

