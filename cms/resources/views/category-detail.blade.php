@extends('layouts.master')

@section('title','Category')

@section('content')
<div class="card text-center">
  <div class="card-header">
    {{ $category->nama_category }}
  </div>
  <div class="card-body">
    <p class="card-text">{{ $category->slug }}</p>
    <p class="card-text">{{ $category->description }}</p>
  </div>
</div>
@endsection

