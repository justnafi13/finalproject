@extends('layouts.master')

@section('title','Article')

@section('content')
<div class="card">
    <div class="card-header">
    <h3 class="card-title">Data Article</h3>
    </div>
    <!-- /.card-header -->
    <div class="card-body">
    <div class="">
        <a href="{{ url('article/create') }}" class="btn btn-primary mb-3" role="button">Tambah Data</a>
    </div>
    <table id="example1" class="table table-bordered table-striped">
        <thead>
            <tr>
                <th>Id</th>
                <th>Title</th>
                <th>Slug</th>
                <th>Category</th>
                <th>Created</th>
                <th>Updated</th>
                <th colspan="2" class="text-center">Action</th>
            </tr>
        </thead>
        <tbody>
            @foreach($article as $p)
            <tr>
                <td>{{ $p->id }}</td>
                <td><a href="/article/{{ $p->id }}" target="_blank" rel="noopener noreferrer">{{ $p->title }}</a></td>
                <td>{{ $p->slug }}</td>
                @foreach($p->category as $c)
                <td>{{ $c->nama_category  }}</td>
                @endforeach
                <td>{{ $p->created_at }}</td>
                <td>{{ $p->updated_at }}</td>
                <td>
                    <a class="btn" href="/article/{{ $p->id }}/edit"><i class="fas fa-edit"></i></a>
                </td>
                <td>
                <form action="/article/{{ $p->id }}" method="POST">
                @csrf
                    <input type="hidden" name="_method" value="DELETE">
                    <button class="btn"><i class="fas fa-eraser"></i></button>
                </form>
                </td>
            </tr>
            @endforeach
        </tbody>
        </table>
        </div>
        <!-- /.card-body -->
</div>
@endsection