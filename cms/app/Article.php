<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Article extends Model
{
    protected $table = "article";

    protected $guarded= [];

    public function category()
    {
    	return $this->belongsToMany('App\Category', 'category_article', 'article_id', 'category_id');
    }
}
