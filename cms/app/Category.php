<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    protected $table = "category";
    protected $fillable = ['nama_category', 'slug', 'description'];
    public function article()
    {
    	return $this->belongsToMany('App\Article');
    }
}
