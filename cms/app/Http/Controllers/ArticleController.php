<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Category;
use App\Article;
use App\CategoryArticle;

class ArticleController extends Controller
{
    public function index()
    {
        // $data['article'] = Article::orderBy('created_at')->get();
        // // 
        $data['article'] = Article::with(['category'])->get();
        
        return view('article', $data);
    }

    public function create()
    {
        $data['category'] = Category::get();

        return view('article-input', $data);
    }

    public function store(Request $request)
    {

        // dd($request->all());
        $this->validate($request, [
            'title'         => 'required',
            'slug'          => 'required',
            'content'       => 'required',
            'img'           =>  'image|nullable|max:199'
        ]);
        
        
        $article = Article::create([
            'title' => $request->title,
            'slug' => $request->slug,
            'content' => $request->content,
            'admin_id' => $request->user()->id
        ]);

        CategoryArticle::create([
            'category_id'   => $request->category,
            'article_id'    => $article->id   
        ]);

        return redirect('article')
            ->with('succes','Account berhasil dibuat');
    }

    public function show($id)
    {
        $data['article'] = Article::find($id);
        // dd($data);
        return view('article-detail', $data);
    }
    
    public function edit($id)
    {
        $data['article'] = Article::with(['category'])->where('id',$id)->first();
        $data['category'] = Category::all();
        // dd($data);
        return view('article-edit', $data);
    }

    public function update(Request $request, $id)
    {
        // dd($request->all());
        $this->validate($request, [
            'title'         => 'required',
            'slug'          => 'required',
            'content'       => 'required',
        ]);
        
        // $article = Article::create([
        //     'title' => $request->title,
        //     'slug' => $request->slug,
        //     'content' => $request->content,
        //     'admin_id' => 
        // ]);

        // CategoryArticle::find([
        //     'category_id'   => $request->category,
        //     'article_id'    => $article->id   
        // ]);
    
        $article = Article::find($id);
        $article->title = $request->title;
        $article->slug = $request->slug;
        $article->content = $request->content;
        $article->save(); 

        
        $category = CategoryArticle::where('article_id', $article->id)->get();
        
        if(count($category)){
            foreach($category as $cat)
            {
                $cat->delete();
            }
        }
        
        $newCat = new CategoryArticle;
        $newCat->category_id = $request->category;
        $newCat->article_id = $article->id;
        $newCat->save();

        // dd($article->category, $newCat);
        return redirect('article')
            ->with('succes','Article berhasil diubah');
    }

    public function destroy($id)
    {
        // dd('stop');
        $data['article'] = Article::find($id)->delete();
        // dd($pertanyaan); 
        return redirect('article');
    }
}
