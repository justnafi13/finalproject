<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Category;
use App\Article;

class CategoryController extends Controller
{
    public function index()
    {
        $data['category'] = Category::orderBy('created_at')->get();
        // dd($data);
        return view('category', $data);
    }

    public function create()
    {
        return view('category-input');
    }

    public function store(Request $request)
    {
        // dd($request->all());
        $this->validate($request, [
            'nama_category'         => 'required',
            'slug'                  => 'required',
            'description'           => 'required',
        ]);

        Category::create([
            'nama_category' => $request->nama_category,
            'slug' => $request->slug,
            'description' => $request->description
        ]);
        return redirect('category')
            ->with('succes','Account berhasil dibuat');
    }

    public function show($id)
    {
        $data['category'] = Category::find($id);
        // dd($data);
        return view('category-detail', $data);
    }
    
    public function edit($id)
    {
        $data['category'] = Category::find($id);
        // dd($data);
        return view('category-edit', $data);
    }

    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'nama_category'         => 'required',
            'slug'        => 'required',
            'description'     => 'required',
        ]);

        $category = Category::find($id);
        $category->nama_category    = $request->nama_category;
        $category->slug             = $request->slug;
        $category->description      = $request->description;
        $category->save();

        return redirect('category')
            ->with('succes','Account berhasil diubah');
    }

    public function destroy($id)
    {
        // dd('stop');
        $data['category'] = Category::find($id)->delete();
        // dd($pertanyaan); 
        return redirect('category');
    }
}