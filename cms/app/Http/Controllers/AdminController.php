<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;

class AdminController extends Controller
{
    public function index()
    {
        $data['administrator'] = User::orderBy('created_at')->get();
        // dd($data);
        return view('admin', $data);
    }
    
    public function create()
    {
        return view('admin-input');
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'name'         => 'required',
            'email'        => 'required',
            'password'     => 'required',
        ]);

        User::create([
            'name' => $request->name,
            'email' => $request->email,
            'password' => bcrypt($request->password)
        ]);
        return redirect('admin')
            ->with('succes','Account berhasil dibuat');
    }

}
