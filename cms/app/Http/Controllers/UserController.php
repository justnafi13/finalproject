<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\WebUser;

class UserController extends Controller
{
    public function index()
    {
        $data['users'] = WebUser::orderBy('created_at')->get();
        // dd($data);
        return view('users', $data);
    }

    public function create()
    {
        return view('users-input');
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'name'         => 'required',
            'email'        => 'required',
            'password'     => 'required',
        ]);

        WebUser::create([
            'name' => $request->name,
            'email' => $request->email,
            'password' => bcrypt($request->password)
        ]);
        return redirect('users')
            ->with('succes','Account berhasil dibuat');
    }

    // public function show($id)
    // {
    //     $data['users'] = WebUser::find($id);
    //     // dd($data);
    //     return view('users-detail', $data);
    // }
    
    public function edit($id)
    {
        $data['users'] = WebUser::find($id);
        // dd($data);
        return view('users-edit', $data);
    }

    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'name'         => 'required',
            'email'        => 'required',
            'password'     => 'required',
        ]);

        $user = WebUser::find($id);
        $user->name = $request->name;
        $user->email = $request->email;
        $user->password = bcrypt($request->password);
        $user->save();

        return redirect('users')
            ->with('succes','Account berhasil diubah');
    }

    public function destroy($id)
    {
        // dd('stop');
        $data['users'] = WebUser::find($id)->delete();
        // dd($pertanyaan); 
        return redirect('users');
    }
}
