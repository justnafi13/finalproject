<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CategoryArticle extends Model
{
    protected $table="category_article";

    protected $fillable = [
        'category_id', 'article_id', 'admin_id',
    ];
}
