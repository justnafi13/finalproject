-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Sep 20, 2020 at 07:41 AM
-- Server version: 10.4.13-MariaDB
-- PHP Version: 7.4.7

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `collabs`
--

-- --------------------------------------------------------

--
-- Table structure for table `administrator`
--

CREATE TABLE `administrator` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `administrator`
--

INSERT INTO `administrator` (`id`, `name`, `email`, `email_verified_at`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'papagaul', 'papagaul@gmail.com', NULL, '$2y$10$TjHBDO5Z8SCfitG8b6/NC.H9PFXTIWU7oIMdYTqXj/P5nIeLBxjrO', NULL, '2020-09-19 22:30:30', '2020-09-19 22:30:30');

-- --------------------------------------------------------

--
-- Table structure for table `article`
--

CREATE TABLE `article` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `content` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `admin_id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `img` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `article`
--

INSERT INTO `article` (`id`, `title`, `slug`, `content`, `admin_id`, `created_at`, `updated_at`, `img`) VALUES
(1, 'Chocolate High with Almond Milk', 'chocolat High Almond', 'Banana, Oatmeal, Natural Peanut Butter, Whey Protein Chocolate, Almond, RO Water, a dash of Sea Salt\r\n\r\nWe guarantee you’ll be blown-a-whey! With high levels of dietary fiber, vitamin C, magnesium, and other essential minerals, this low calorie and high protein drink is suitable as a meal replacement while also keeping your bones strong!\r\n\r\nHigh in vitamin C  I  Source of protein  I  Source of fiber  I  Source of magnesium  I  Source of phosphor', 1, '2020-09-19 22:34:14', '2020-09-19 22:34:14', NULL),
(2, 'Curcuma Ginger SHOT', 'curcuma_gingger', 'Ginger, Curcuma, Green Apple, Lemon Curcuma Ginger SHOT is high in vitamin C and has great inflammatory benefits. This shot is slightly fiery and immediately wakes you up! High in vitamin C  I  High in fiber', 1, '2020-09-19 22:35:28', '2020-09-19 22:35:28', NULL),
(3, 'Green Glory', 'greenglory', 'Organic Spinach, Organic Celery Stick, Pineapple, Orange\r\n\r\nPacks a bunch of strong flavors, we’ve combined the highly alkalizing organic spinach and organic celery with the natural sweetness of fresh orange and pineapple for a recipe that helps lower blood pressure and cholesterol levels.\r\n\r\nHigh in vitamin C & K  I  Source of vitamin B9  I  High in fiber  I  High in iron  I  Source of Calcium  I  High in manganese', 1, '2020-09-19 22:36:13', '2020-09-19 22:36:13', NULL),
(4, 'Glowing Golden', 'Glowing Golden', 'Organic Carrot, Pineapple, Apple, Turmeric\r\n\r\nWith organic carrot as its base, our Glowing Golden containing curcumin from turmeric, it has anti-inflammatory and antioxidants properties that helps to strengthen immune system and maintain digestive health.\r\n\r\nSource of vitamin B6  I  High in fiber  I  Source of manganese', 1, '2020-09-19 22:37:12', '2020-09-19 22:37:12', NULL),
(5, 'i.Glow', 'i.Glow', 'Organic Carrot, Pineapple, Apple\r\n\r\nFilled with fiber, beta carotene and antioxidants, this mix of organic carrot, pineapple, and apple will give you the energy you need to stay refreshed and energized throughout the day while also improving your eyesight!\r\n\r\nHigh in vitamin K  I  High in fiber  I  Source of manganese', 1, '2020-09-19 22:38:09', '2020-09-19 22:38:09', NULL),
(6, 'Minty Apple', 'Minty Apple', 'Green Apple, Pineapple, Mint\r\n\r\nThis delicious yet healthy mix of pineapple, juicy green apples, and mint improves your metabolism and maintains digestive health. The Minty Apple blend makes for a juice that is refreshing yet cooling.\r\n\r\nHigh in vitamin B6  I  Source of vitamin C  I  High in fiber', 1, '2020-09-19 22:39:16', '2020-09-19 22:39:16', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `category`
--

CREATE TABLE `category` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `nama_category` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `category`
--

INSERT INTO `category` (`id`, `nama_category`, `slug`, `description`, `created_at`, `updated_at`) VALUES
(1, 'Signature Line', 'signature_line', 'Using 100% Organic Vegetables, this line contains more vegetables, more antioxidants, and more nutrients.', '2020-09-19 22:31:44', '2020-09-19 22:31:44'),
(2, 'Classic Line', 'classic_line', 'Cold-Pressed to perfection, this line is customer\'s all-time favorite Showing all 18 results', '2020-09-19 22:32:14', '2020-09-19 22:32:14'),
(3, 'Smoothies', 'smoothies', 'Who said breakfast only comes in a bowl? Our Smoothies are the perfect meal replacement with up to 32 g of proteins and 10 g of fiber that guarantees you reach for seconds after just one sip. Showing all 4 results', '2020-09-19 22:32:48', '2020-09-19 22:32:48');

-- --------------------------------------------------------

--
-- Table structure for table `category_article`
--

CREATE TABLE `category_article` (
  `category_id` bigint(20) UNSIGNED NOT NULL,
  `article_id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `category_article`
--

INSERT INTO `category_article` (`category_id`, `article_id`, `created_at`, `updated_at`) VALUES
(1, 1, '2020-09-19 22:34:14', '2020-09-19 22:34:14'),
(1, 2, '2020-09-19 22:35:28', '2020-09-19 22:35:28'),
(1, 3, '2020-09-19 22:36:13', '2020-09-19 22:36:13'),
(2, 4, '2020-09-19 22:37:12', '2020-09-19 22:37:12'),
(3, 5, '2020-09-19 22:38:09', '2020-09-19 22:38:09'),
(3, 6, '2020-09-19 22:39:16', '2020-09-19 22:39:16');

-- --------------------------------------------------------

--
-- Table structure for table `commentar`
--

CREATE TABLE `commentar` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `article_id` bigint(20) UNSIGNED NOT NULL,
  `comment` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `failed_jobs`
--

CREATE TABLE `failed_jobs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2019_08_19_000000_create_failed_jobs_table', 1),
(4, '2020_09_18_043749_create_administrator_table', 1),
(5, '2020_09_18_133231_create_category_table', 1),
(6, '2020_09_18_133539_create_article_table', 1),
(7, '2020_09_18_133611_create_commentar_table', 1),
(8, '2020_09_18_133635_create_category_article_table', 1),
(9, '2020_09_19_032634_add_img_to_article', 1),
(10, '2020_09_19_180331_add_timestamp_to_category_article', 1);

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `administrator`
--
ALTER TABLE `administrator`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `administrator_email_unique` (`email`);

--
-- Indexes for table `article`
--
ALTER TABLE `article`
  ADD PRIMARY KEY (`id`),
  ADD KEY `article_admin_id_foreign` (`admin_id`);

--
-- Indexes for table `category`
--
ALTER TABLE `category`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `category_article`
--
ALTER TABLE `category_article`
  ADD KEY `category_article_category_id_foreign` (`category_id`),
  ADD KEY `category_article_article_id_foreign` (`article_id`);

--
-- Indexes for table `commentar`
--
ALTER TABLE `commentar`
  ADD PRIMARY KEY (`id`),
  ADD KEY `commentar_user_id_foreign` (`user_id`),
  ADD KEY `commentar_article_id_foreign` (`article_id`);

--
-- Indexes for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `administrator`
--
ALTER TABLE `administrator`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `article`
--
ALTER TABLE `article`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `category`
--
ALTER TABLE `category`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `commentar`
--
ALTER TABLE `commentar`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `article`
--
ALTER TABLE `article`
  ADD CONSTRAINT `article_admin_id_foreign` FOREIGN KEY (`admin_id`) REFERENCES `administrator` (`id`);

--
-- Constraints for table `category_article`
--
ALTER TABLE `category_article`
  ADD CONSTRAINT `category_article_article_id_foreign` FOREIGN KEY (`article_id`) REFERENCES `article` (`id`),
  ADD CONSTRAINT `category_article_category_id_foreign` FOREIGN KEY (`category_id`) REFERENCES `category` (`id`);

--
-- Constraints for table `commentar`
--
ALTER TABLE `commentar`
  ADD CONSTRAINT `commentar_article_id_foreign` FOREIGN KEY (`article_id`) REFERENCES `article` (`id`),
  ADD CONSTRAINT `commentar_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
