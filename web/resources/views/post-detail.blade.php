@extends('layouts.artcl')

@section('title','detail')

@section('content')
<article class="flex lg:h-screen w-screen lg:overflow-hidden xs:flex-col lg:flex-row">
    <div class="relative lg:w-1/2 xs:w-full xs:h-84 lg:h-full post-left">
        <img src="{{ asset('img/beatthat.jpeg') }}"  class="absolute h-full w-full object-cover">
        <div class="overlay"></div>
        <div class="absolute top-32 left-32 text-white">
            <a href="{{ url('/')}}"><h1 class="font-bold text-4xl" >Collabs</h1></a>
            <div class="mt-16 -mb-3 flex uppercase text-sm">
                <p class="mr-3">
                {{ $article->created_at->format('F d, Y') }}
                </p>
                <span class="mr-3">•</span>
                <p> {{ $article->author->name }} </p>
            </div>
            <h1 class="text-6xl font-bold">{{ $article->title }}</h1>
            @foreach($article->category as $cat)
            <span>
                <a href="/category/{{$cat->id}}" class="">
                    <span class="truncate uppercase tracking-wider font-medium text-ss px-2 py-1 rounded-full mr-2 mb-2 border border-light-border dark:border-dark-border transition-colors duration-300 ease-linear">
                        {{ $cat->nama_category }}
                    </span>
                </a>
            </span>
            @endforeach
        </div>
        <div class="flex absolute top-3rem right-3rem">
            <a href="{{ url('/') }}" class="mr-8 self-center text-white font-bold hover:underline">All article</a>
            @include('layouts/partials.search')
        </div>
    </div>
    <div class="relative xs:py-8 xs:px-8 lg:py-32 lg:px-16 lg:w-1/2 xs:w-full h-full overflow-y-scroll markdown-body post-right custom-scroll">
        <h1 class="font-bold text-4xl">{{ $article->title }}</h1>
        <p class="pb-4">{{ $article->created_at->format('F d, Y')}}</p>
        <!-- table of contents -->
        <!-- content from markdown -->
        <div class="my-5">
            {!! $article->content !!}
        </div>
        <!-- content author -->
        <div class="w-full px-2 xs:mb-6 md:mb-12 article-card">
            <div class="flex transition-shadow duration-150 ease-in-out shadow-sm hover:shadow-md xxlmax:flex-col">
                <img class="h-48 xxlmin:w-1/2 xxlmax:w-full object-cover" src="{{asset('img/beatthat.jpeg') }}" />
                <div class="flex flex-col m-4">
                    <h4 class="font-semibold">Author</h4>
                    <p class="uppercase">{{ $article->author->name }}</p>
                </div>
            </div>
        </div>
        <!-- prevNext -->

        @guest
        Please <a href="{{ route('login') }}">Login</a> First 
        @else
        @include('layouts/partials.comentarform')
        @endguest
        <div class="py-4">
            @foreach($article->comment as $com)
            <div class="flex mx-auto items-center justify-center shadow-lg mt-5 mx-8 mb-4 max-w-lg">
                <div class="py-8 px-4 bg-white  rounded-b-md fd-cl group-hover:opacity-25">
                    <span class="block text-lg text-gray-800 font-bold tracking-wide">{{ $com->user->name}}</span>
                    <span  class="block text-gray-600 text-sm">{{ $com->comment }}</span>
                </div>
            </div>
            @endforeach
        </div>
    </div>
</article>
@endsection