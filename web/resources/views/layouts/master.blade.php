<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">

    <title>@if(View::hasSection('title')) @yield('title') - @endif</title>
    
    <link rel="stylesheet" href="{{ asset('css/app.css') }}">
    <link rel="stylesheet" href="{{ asset('style.css') }}">

    @yield('style')

</head>
<body>
    <div class="m-8">
        @include('layouts/partials.header')

        <div class="my-5">

        </div>

        @yield('content')

        @include('layouts/partials.footer')

    </div class="m-8">

   <script src="https://cdn.jsdelivr.net/gh/alpinejs/alpine@v2.x.x/dist/alpine.min.js" defer></script>
   
   @yield('script')
   <script src="//cdn.ckeditor.com/4.14.0/standard/ckeditor.js"></script>
</body>
</html>