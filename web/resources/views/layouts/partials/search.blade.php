<div class="ml-8">
    <form action="{{ url('search') }}" >
        <input type="search" autocomplete="off" name="cari" placeholder="Search Articles" class="block w-full pl-10 pr-3 py-2 truncate leading-5 placeholder-gray-500 border border-gray-500 text-gray-700 focus:border-gray-300 rounded-md focus:outline-none focus:bg-white bg-white">
    </form>
</div>