@extends('layouts.master')

@section('title','Home')

@section('content')
<ul class="flex flex-wrap">
   @foreach($article as $ar)
   <li class="xs:w-full md:w-1/2 px-2 xs:mb-6 md:mb-12 article-card">
      <a href="/detail/{{ $ar->id }}" class="flex transition-shadow duration-150 ease-in-out shadow-sm hover:shadow-md xxlmax:flex-col">
         <img src="{{ asset('img/beatthat.jpeg') }}" class="h-48 xxlmin:w-1/2 xxlmax:w-full object-cover"/>
         <div class="p-6 flex flex-col justify-between xxlmin:w-1/2 xxlmax:w-full">
            <h2 class="font-bold">{{ $ar->title}}</h2>
            <p class="uppercase">{{ $ar->author->name }}</p>
            <div class="text-gray-600 text-sm">
            {!! substr(strip_tags($ar->content),0 ,20) !!}
            </div>
         </div>
      </a>
   </li>
   @endforeach
</ul>
@endsection