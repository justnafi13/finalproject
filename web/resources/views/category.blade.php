@extends('layouts.artcl')

@section('title','Category')

@section('content')
  <div class="flex lg:h-screen w-screen lg:overflow-hidden xs:flex-col lg:flex-row">
    <div class="relative lg:w-1/2 xs:w-full xs:h-84 lg:h-full post-left">
      <img src="{{ asset('img/beatthat.jpeg') }}"
        class="absolute h-full w-full object-cover">
    </div>

    <div class="overlay"></div>
    <div class="absolute top-32 left-32 text-white">
        <a href="/" aria-current="page" class="nuxt-link-exact-active nuxt-link-active">
            <h1 class="font-bold text-4xl" >Collabs</h1>
        </a>
        <div class="mt-16 -mb-3 flex flex-col uppercase text-sm">
            <h1 class="text-4xl font-bold">
            {{ $category->nama_category }}
            </h1>
            <p class="mb-4">{{ $category->description }}</p>
        </div>
    </div>
    <div class="relative xs:py-8 xs:px-8 lg:py-32 lg:px-16 lg:w-1/2 xs:w-full h-full overflow-y-scroll markdown-body post-right custom-scroll">
        <a href="{{ url('/') }}"><p class="hover:underline">Back to All Articles</p></a>  
        <h3 class="mb-4 font-bold text-4xl">
            Here are a list of articles by {{ $category->nama_category }} :
        </h3>
      <ul>
        @foreach($category->article as $a)
        <li class="w-full px-2 xs:mb-6 md:mb-12 article-card">
          <a href="/detail/{{$a->id}}" class="flex transition-shadow duration-150 ease-in-out shadow-sm hover:shadow-md xxlmax:flex-col">
            <img src="{{ asset('img/beatthat.jpeg') }}" class="h-48 xxlmin:w-1/2 xxlmax:w-full object-cover">
            <div class="p-6 flex flex-col justify-between xxlmin:w-1/2 xxlmax:w-full">
              <h2 class="font-bold">{{ $a->title }}</h2>
              <p>{{ $a->content }}</p>
              <p class="font-bold text-gray-600 text-sm">
                {{ $a->created_at->format('F d, Y') }}
              </p>
            </div>
          </a>
        </li>
        @endforeach
      </ul>
    </div>
  </div>
@endsection