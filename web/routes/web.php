<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/home', function () {
//     return view('homepage');
// });
// Route::get('/', function () {
//     return view('homepage');
// });

Route::get('password/reset/{token}', 'ResetPasswordController@showResetForm')->name('password.request');

Route::get('/', 'HomeController@index')->name('home');
Route::get('/home', 'HomeController@index')->name('home');
Route::get('/search', 'HomeController@search');
Route::get('/detail/{id}', 'HomeController@show');
Route::post('/detail/{id}', 'HomeController@comment');
Route::get('/category/{id}', 'HomeController@listCategory');

Auth::routes();
