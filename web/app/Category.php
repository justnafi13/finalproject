<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    protected $table = "category";
 
    public function article()
    {
    	return $this->belongsToMany('App\Article', 'category_article' , 'category_id' , 'article_id');
    }
}
