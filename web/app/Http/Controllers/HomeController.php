<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Author;
use App\Category;
use App\Article;
use App\Komentar;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $data['article'] = Article::orderBy('created_at')->get();

        return view('homepage', $data);
    }

    public function show($id)
    {
        $data['article'] = Article::with(['comment', 'comment'])->where('id', $id)->first();
        // dd($data);
        return view('post-detail', $data);
    }

    public function search(Request $request)
    {
        // dd($request->all());
        $cari = $request->cari;
        $data['article'] = Article::where('title', 'LIKE', '%' . $cari. '%')->get();

        return view('search', $data);
    }

    public function listCategory($id)
    {
        $data['category'] = Category::with(['article', 'article'])->where('id',$id)->first();

        return view('category', $data);
    }

    public function comment(Request $request, $id)
    {
        // dd($request->all());
        // dd($id);
        $this->validate($request, [
            'comment' => 'required',
        ]);

        Komentar::create([
            'user_id'       => $request->user_id,
            'article_id'    => $id,
            'comment'       => $request->comment,
        ]);
        return redirect()
            ->back()->with('succes','Pertanyaan berhasil dibuat');
    }
}
