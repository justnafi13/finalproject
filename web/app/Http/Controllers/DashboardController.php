<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class DashboardController extends Controller
{
    public function index()
    {
        return view('post-detail');
    }

    public function show()
    {
        return view('category');
    }
}
