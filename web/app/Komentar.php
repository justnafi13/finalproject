<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

use App\User;
use App\Article;

class Komentar extends Model
{
    protected $table= 'commentar';
  
    protected $guarded = [];

    public function user(){
    	return $this->BelongsTo('App\User', 'user_id');
    }

    public function article(){
    	return $this->BelongsTo('App\Author', 'article_id');
    }
}
