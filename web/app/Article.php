<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Komentar;

class Article extends Model
{
    protected $table = "article";

    public function author(){
    	return $this->BelongsTo('App\Author', 'admin_id');
    }
 
    public function category()
    {
    	return $this->belongsToMany('App\Category', 'category_article', 'category_id', 'article_id');
    }

    public function comment(){
    	return $this->hasMany('App\Komentar');
    }
}
